(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.a8= {
    attach: function(context, settings) {
////////////////////////////////////

//Toggle
const trigger_toggle = document.getElementById('toggle');
const trigger_point = document.getElementById('site');
const trigger_closer = document.getElementById('toggleClose');

trigger_toggle.addEventListener('click', function(e) {
  e.preventDefault();
  trigger_point.classList.toggle('menu-is-open');
});
trigger_closer.addEventListener('click', function(e) {
  trigger_point.classList.remove('menu-is-open');
});

if (document.body.classList.contains('.node-9')){
    document.querySelector('.seudo-blk').setAttribute('id', 'unete');
}
if (document.body.classList.contains('node-10')){
    document.querySelector('.seudo-blk').setAttribute('id', 'ahora');
}

//sticky
const nav = document.querySelector('#headpage');
//const navTop = nav.offsetTop;
function stickyNavigation() { 

  if (window.scrollY >= 140) { /* navTop */
    document.body.classList.add('headpage-class');
  } else { 
    document.body.classList.remove('headpage-class');
  }
};
window.addEventListener('scroll', stickyNavigation);
//////////////////////////////////
    },
    detach: function (context, settings, trigger) {
///////////////////////////////////

//////////////////////////////////
    }
  };

})(jQuery, Drupal);